package com.s2h.manage.webProducts.rpc.rmi.service;

import com.s2h.manage.webProducts.rpc.rmi.IRmi;

public interface IRmiServiceB extends IRmi {
	public String test_v1();

	public String test_v2(String str);
}
