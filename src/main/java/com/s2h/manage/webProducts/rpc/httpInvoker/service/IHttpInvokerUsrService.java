package com.s2h.manage.webProducts.rpc.httpInvoker.service;

import com.s2h.manage.webProducts.rpc.httpInvoker.IHttpInvoker;

public interface IHttpInvokerUsrService extends IHttpInvoker {
	public String queryUsrs();

	public String sayHelloToSomeBody(String someBodyName);
}
