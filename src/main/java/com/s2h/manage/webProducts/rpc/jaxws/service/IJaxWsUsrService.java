package com.s2h.manage.webProducts.rpc.jaxws.service;

import javax.jws.WebService;

import com.s2h.manage.webProducts.rpc.jaxws.IJaxWs;

@WebService(serviceName = "usrService")
public interface IJaxWsUsrService extends IJaxWs {

	public String queryUsrs();

}
