package com.s2h.manage.webProducts.rpc.jaxws.service;

import javax.jws.WebService;

import com.s2h.manage.webProducts.rpc.jaxws.IJaxWs;

@WebService(serviceName = "logService")
public interface IJaxWsLogService extends IJaxWs {

	public String queryLogs();

}
