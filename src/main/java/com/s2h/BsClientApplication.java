package com.s2h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsClientApplication.class, args);
	}
}
